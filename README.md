# modern-navbar
<!--<img src="https://user-images.githubusercontent.com/70860732/113480469-47734100-948c-11eb-9818-eaf5b7c4044e.jpeg" width="50%" align="right">-->
<!--<img src="https://user-images.githubusercontent.com/70860732/113482445-b3f33d80-9496-11eb-83cd-b0ed2b02173e.gif" width="" align="right">-->
<img src="https://user-images.githubusercontent.com/70860732/113486429-916b1f80-94aa-11eb-82bb-076ae4baca86.gif" width="50%" align="right">

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/modern-navbar/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/modern-navbar?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/modern-navbar?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/modern-navbar?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/modern-navbar)](https://replit.com/@KennyOliver/modern-navbar)

**Modern navbar made using CSS**

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/modern-navbar)](https://kennyoliver.github.io/modern-navbar)

---
Kenny Oliver ©2021
